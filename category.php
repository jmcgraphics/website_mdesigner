<?php get_header('fija'); ?>


<div class="container" id="categoria-container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="col-">
                <?php if ( is_category() ) : ?>
                <h2 id="category-name-header ">
                    <?php $categoria = get_the_category();
                            $parent = get_cat_name($categoria[0]->category_parent);
                            if (!empty($parent)) {
                                echo $parent;
                            } else {
                                echo $categoria[0]->cat_name;
                            } ?>
                </h2>
                <?php endif; ?>
                <hr>
            </div>
            <?php $relate = get_posts( array( 'category__in' => wp_get_post_categories($post->ID),'numberposts'   => 10, 'post__not_in' => array($post->ID) ) ); ?>

            <?php if ($relate) foreach( $relate as $post){ setup_postdata($post);?> 
            <div class="row mb-3">
                <div class="col">
                    <div class="media shadow-1">
                        <a class="" href="<?php the_permalink();?>">
                            <?php if ( has_post_thumbnail() ):  { the_post_thumbnail( 'mdesigner_category',array('class' => 'd-flex align-self-start mr-3') ); } ?>
                            <?php else : ?>
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/DEFAULT-IMG64x64.jpg" class="d-flex align-self-start mr-3" alt="mdesigner-default">
                            <?php endif; ?>
                        </a>
                        <div class="media-body">
                            <h3 class="mt-0">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title(); ?>
                                </a><small class="text-muted"><br> <?php $time = human_time_diff( get_the_time('U') , current_time('timestamp') );
                                echo sprintf( __( 'hace %s', 'dominio' ), $time ); ?></small>
                            </h3>
                            <small>Categoría: <?php the_category(', '); ?></small>
                                <?php the_excerpt(); ?>
                                <a class="btn"href="<?php the_permalink() ?>">
                                    Visitar
                                </a>
                            
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                    
                    } wp_reset_postdata();  ?>
        </div>
        <div class="hidden-sm-down col-md-3" id="sidebar">
            <?php if( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_1')):?>
            <?php endif; ?>
        </div>
    </div>
    <br>
</div>

<?php get_footer(); ?>
