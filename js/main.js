$(document).ready(function () {



    var s, e = !1;
    $(window).scroll(function () {
            s = $(window).scrollTop(), s > 200 ? e || ($(".navbar.ppal").css({
                "background-color": "#FFFFFF",
                transition: "background-color 0.5s ease-in"
            }), $("nav").addClass("shadow-1"), $("ul .dropdown-menu").addClass("shadow-1"), e = !0) : e && ($(".navbar.ppal").css({
                "background-color": "transparent",
                transition: "background-color 0.5s ease-out"
            }), $("nav").removeClass("shadow-1"), $("ul .dropdown-menu").removeClass("shadow-1"), e = !1)
        }), $(".carousel").carousel({
            interval: 2e3
        }),



        // Inicio Llamado a instagram
        function (e) {
            e(function () {
                e(".instagram-lite").instagramLite({
                    accessToken: "4768232846.1677ed0.489498cc28d444f89082865e538f80bb",
                    list: !0,
                    urls: !0,
                    likes: !0,
                    limit: "6",
                    success: function () {
                        console.log("The request was successful!")
                    },
                    error: function (e, s) {
                        console.log("There was an error with the request")
                    }
                })
            }), e.fn.instagramLite = function (s) {
                if (!this.length) return this;
                plugin = this, plugin.defaults = {
                    accessToken: null,
                    user_id: "self",
                    limit: null,
                    list: !0,
                    videos: !1,
                    urls: !1,
                    captions: !1,
                    date: !1,
                    likes: !1,
                    comments_count: !1,
                    error: function () {},
                    success: function () {}
                };
                var t = e.extend({}, plugin.defaults, s),
                    a = e(this),
                    o = function (e) {
                        for (var s = e.split(" "), t = "", a = 0; a < s.length; a++) {
                            var o;
                            if ("@" == s[a][0]) {
                                var i = '<a href="http://instagram.com/' + s[a].replace("@", "").toLowerCase() + '" target="_blank">' + s[a] + "</a>";
                                o = i
                            } else if ("#" == s[a][0]) {
                                var i = '<a href="http://instagram.com/explore/tags/' + s[a].replace("#", "").toLowerCase() + '" target="_blank">' + s[a] + "</a>";
                                o = i
                            } else o = s[a];
                            t += o + " "
                        }
                        return t
                    },
                    i = function (e) {
                        for (var s = 0; s < e.length; s++) {
                            var n, i = e[s];
                            if ("image" === i.type || "carousel" === i.type || !t.videos) {
                                if (n = '<img class="elevation-1 il-photo__img" src="' + i.images.standard_resolution.url + '" alt="Instagram Image" data-filter="' + i.filter + '" />', t.urls && (n = '<a href="' + i.link + '" target="_blank">' + n + "</a>"), (t.captions || t.date || t.likes || t.comments_count) && (n += '<div class="il-photo__meta">'), t.captions && i.caption && (n += '<div class="il-photo__caption" data-caption-id="' + i.caption.id + '">' + o(i.caption.text) + "</div>"), t.date) {
                                    var r = new Date(1e3 * i.created_time),
                                        l = r.getDate(),
                                        c = r.getMonth() + 1,
                                        d = r.getFullYear();
                                    r.getHours(), r.getMinutes(), r.getSeconds();
                                    r = c + "/" + l + "/" + d, n += '<div class="il-photo__date">' + r + "</div>"
                                }
                                t.likes && (n += '<div class="il-photo__likes">' + i.likes.count + "</div>"), t.comments_count && i.comments && (n += '<div class="il-photo__comments">' + i.comments.count + "</div>"), (t.captions || t.date || t.likes || t.comments_count) && (n += "</div>")
                            }
                            if ("video" === i.type && t.videos && i.videos) {
                                var m;
                                i.videos.standard_resolution ? m = i.videos.standard_resolution.url : i.videos.low_resolution ? m = i.videos.low_resolution.url : i.videos.low_bandwidth && (m = i.videos.low_bandwidth.url), n = '<video poster="' + i.images.standard_resolution.url + '" controls>', n += '<source src="' + m + '" type="video/mp4;"></source>', n += "</video>"
                            }
                            t.list && n && (n = '<li class="il-item" data-instagram-id="' + i.id + '">' + n + "</li>"), "" !== n && a.append(n)
                        }
                    },
                    n = function () {
                        if (t.accessToken) {
                            var s = "https://api.instagram.com/v1/users/" + t.user_id + "/media/recent/?access_token=" + t.accessToken + "&count=" + t.limit;
                            e.ajax({
                                type: "GET",
                                url: s,
                                dataType: "jsonp",
                                success: function (e) {
                                    200 === e.meta.code && e.data.length ? (i(e.data), t.success.call(this)) : t.error.call(this)
                                },
                                error: function () {
                                    t.error.call(this)
                                }
                            })
                        }
                    };
                n()
            }
        }

    // Fin Llamado a instagram

});
