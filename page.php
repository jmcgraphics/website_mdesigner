<?php get_header('fija') ?>

<div class="container content">
    <div class="row">
        <div class="col">
            <?php if (have_posts()) : while (have_posts()) : the_post();?>
             <div class="post">
              <div class="entrytext">
               <?php the_content(); ?>
              </div>
             </div>
             <?php endwhile; endif; ?>
        </div>
    </div>
   
</div>

<?php get_footer() ?>