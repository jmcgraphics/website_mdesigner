<?php get_header('fija'); ?>
    <!-- content-->
    <div class="container content">
       <div class="row justify-content-center">
           <div class="container">
               <?php if (have_posts()) : while (have_posts()) : the_post();?>
                 <div class="row post">
                     <div class="col">
                        <?php if ( has_post_thumbnail() ):  { the_post_thumbnail( 'medium' ); } ?>
                        <?php else : ?>
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/DEFAULT-IMG.jpg" class="img-default" alt="mdesigner-default">
                        <?php endif; ?>    
                     </div>
                 <div class="col col-md-8">
                     <h2 id="post-<?php the_ID(); ?>"><?php the_title();?></h2>
                     <h5>
                         <?php the_author(); ?> / <?php the_category(); ?> / <?php the_date(); ?>
                     </h5>
                  <div class="entrytext">
                   <?php the_content('<p class="serif">Leer el resto de esta pagina &raquo;</p>'); ?>
                  </div> 
                 </div>
                 </div>
             <?php endwhile; endif; ?>
             
           </div>
       </div>
       <br>
       <div class="row">
           <div class="col offset-sm-3 comments-container">
             <?php  if ( comments_open() || get_comments_number() ) {
				comments_template();
			} ?>
           </div>
       </div>
       <div class="row ">
           <div class="offset-sm-5 col">
              <div class="navigation input-group mb-2 mr-sm-2 mb-sm-0">
                  <?php previous_post_link(); ?> 
                  <small>/</small>   
                  <?php next_post_link(); ?>
                  
              </div> 
           </div>
       </div>
        
    </div>
<?php get_footer(); ?>