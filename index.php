<?php get_header('fija'); ?>
    <!-- content-->
    <div class="container content">
       <div class="row justify-content-center">
           <div class="col">
               <?php if (have_posts()) : while (have_posts()) : the_post();?>
                 <div class="row post">
                 <?php if (has_post_thumbnail()) : ?>
                 <div class="col-xs-12 col-md-4">
                     <div class="span"><span><?php the_post_thumbnail( '' ); ?></span>
                     </div>
                 </div>
                 <?php endif; ?>
                 <div class="col-xs-12 col-md-8">
                     <h2 id="post-<?php the_ID(); ?>"><?php the_title();?></h2>
                     <h5>
                         <?php the_author(); ?> / <?php the_category(); ?> / <?php the_date(); ?>
                     </h5>
                  <div class="entrytext">
                   <?php the_content('<p class="serif">Leer el resto de esta pagina &raquo;</p>'); ?>
                  </div> 
                 </div>
                 
                 </div>
             <?php endwhile; endif; ?>
             
           </div>
       </div>
       <br>
       <div class="row">
           <div class="col comments-container">
             <?php  if ( comments_open() || get_comments_number() ) {
				comments_template();
			} ?>
           </div>
       </div>
       <div class="row">
           <div class="col-xs-12">
             
              <div class="navigation">
                <h4 class="text-center">
                  <?php previous_post_link(); ?> 
                  <small>or</small>   
                  <?php next_post_link(); ?>
                  
                </h4>
              </div> 
           </div>
       </div>
        
    </div>
<?php get_footer(); ?>