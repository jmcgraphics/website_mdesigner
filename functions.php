<?php 
/* llamar archivo externo para menus bootstrap*/
require_once get_template_directory() . ( '/others/wp-bootstrap-navwalker.php' );
/* /llamar archivo externo para menus bootstrap*/
/* llamar archivo de widgets personalizados */
require_once(get_template_directory().'/others/widgetcard.php');
add_action('widgets_init', 'registra_mis_widgets');
/* /llamar archivo de widgets personalizados */
/* llamar archivo de cambio de login */
require_once(get_template_directory().'/others/Cambiodelogo.php');

/* /llamar archivo de cambio de login */



/* CAmbiar la clase del logo personalizado */
add_filter('get_custom_logo','change_logo_class');

function change_logo_class($html)
{
	$html = str_replace('class="custom-logo"', '', $html);
	$html = str_replace('class="custom-logo-link"', 'class="navbar-brand"', $html);
	return $html;
}
/* CAmbiar la clase del logo personalizado */


/* registra los menus*/

register_nav_menus( array(
		'primary' 	=> __( 'header', 'mdesigner' ),
		'secundary' 	=> __( 'footer', 'mdesigner' ),
	) );
/* /registra los menus*/


/*registrar y añadir logo personalizado */
function mdesigner() {
	
add_theme_support( 'custom-logo', array(
	'height'      => '',
	'width'       => '',
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );

}
add_action( 'after_setup_theme', 'mdesigner' );

function basicthemer_the_custom_logo() {
	
	if ( function_exists( 'custom_logo' ) ) {
		the_custom_logo();
	}

}
/* /registrar y añadir logo personalizado */
/* Header image */
add_theme_support( 'custom-header' );
function mdesigner_custom_header_setup() {
    $defaults = array(
        // Default Header Image to display
        'default-image'         => get_template_directory_uri() . '/images/headers/default.jpg',
        // Display the header text along with the image
        'header-text'           => false,
        // Header text color default
        // Header image width (in pixels)
        'width'             => 1000,
        // Header image height (in pixels)
        'height'            => 198,
        // Header image random rotation default
        'random-default'        => false,
        // Enable upload of image file in admin 
        'uploads'       => false,
        // function to be called in theme head section
        //  function to be called in preview page head section
        'admin-head-callback'       => 'adminhead_cb',
        // function to produce preview markup in the admin screen
        'admin-preview-callback'    => 'adminpreview_cb',
        );
}
add_action( 'after_setup_theme', 'mdesigner_custom_header_setup' );
/* /Header image */

/* Insertar codigo JS*/
add_action("wp_enqueue_scripts", "dcms_insertar_js");

function dcms_insertar_js(){
    wp_register_script('miscript', get_template_directory_uri(). '/js/main.js', array('jquery'), '1', true );
    wp_enqueue_script('miscript');
}

/* /Insertar codigo JS*/



/* soporte de etiqueta titulo wp-tittle & title-tag */

add_action( 'after_setup_theme', 'theme_functions' );
function theme_functions() {
    add_theme_support( 'title-tag' );
}

add_filter( 'wp_title', 'custom_titles', 10, 2 );
function custom_titles( $title, $sep ) {

    //Check if custom titles are enabled from your option framework
    if ( ot_get_option( 'enable_custom_titles' ) === 'on' ) {
        //Some silly example
        $title =  $title . "Some other title" ;
    }

    return $title;
}

/* /soporte de etiqueta titulo wp-tittle & title-tag */

/* the_excerpt opcion de largor*/

function longitud_excerpt($length) {
    return 15;
}
add_filter('excerpt_length', 'longitud_excerpt');
function mi_excerpt_leermas() {
       global $post;
	return '<a href="'. get_permalink($post->ID) . '" > [+]</a>'; 
}
add_filter('excerpt_more', 'mi_excerpt_leermas');

/* /the_excerpt opcion de largor*/


/* widget en area especifica*/
// Register Sidebars
function home() {

	$args = array(
		'id'            => 'homewidget-1',
		'name'          => __( 'Home area'),
		'description'   => __( 'Esta seccion fue diseñada unicamente para aparecer las tarjetas de servicios.' ),
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
		'before_widget' => '<div id="%1$s" class="col-10 col-lg-3 col-sm-6">',
		'after_widget'  => '</div>'
	);
	register_sidebar( $args );

	$args = array(
		'id'            => 'sidebar_1',
		'name'          => __( 'sidebar', 'text_domain' ),
		'before_widget' => '<ol id="%1$s" class="widget %2$s">',
		'after_widget'  => '</ol>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
	);
	register_sidebar( $args );

}
add_action( 'widgets_init', 'home' );

/* /widget en area especifica*/



/*habilita el soporte para imagenes destacadas*/
add_theme_support( 'post-thumbnails' );

add_image_size( 'product-image', 555, 555, true);
add_image_size( 'product-image-relast', 360, 200, true);
add_image_size( 'post-thumbnail', 300, 223, true);
add_image_size( 'mdesigner_featured_image', 750, 400, true);
add_image_size( 'mdesigner_category', 64, 64, true);
remove_action( 'begin_fetch_post_thumbnail_html', '_wp_post_thumbnail_class_filter_add' );
/* /habilita el soporte para imagenes destacadas*/

/* Quitar del front el jquery migrate  */
function dequeue_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}

add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

/* /Quitar del front el jquery migrate  */

/*  Campos personalizados  */



//PRECIO
function save_price() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $price= $_POST['price'];
    update_post_meta($post_id, 'price', $price);
}
add_action('publish_post', 'save_price');

function save_titulo() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $price= $_POST['titulo'];
    update_post_meta($post_id, 'titulo', $price);
}
add_action('publish_post', 'save_titulo');

add_action('admin_menu', 'meta_box_precio');
function meta_box_precio() {
    add_meta_box('precio','Atributos de Producto','el_precio','post','normal','high');
}

function el_precio() {
    
    global $wpdb, $post;
    $value  = get_post_meta($post->ID, 'tproducto', true);
    
    echo '<small>Use esto cuando sea un Producto junto con la plantilla productos</small><br>';
    echo '<label>Tipo de Producto</label><br>
    <input type="text" name="tproducto" id="tproducto" value="'.htmlspecialchars($value).'" style="width: 250px;" /> <br>';
    $value  = get_post_meta($post->ID, 'titulo', true);
    echo '<label>titulo</label><br>
    <input type="text" name="titulo" id="titulo" value="'.htmlspecialchars($value).'" style="width: 100px;" /> <br>';
    $value  = get_post_meta($post->ID, 'precio', true);
    echo '<label>Precio</label><br>
    <input type="text" name="precio" id="precio" value="'.htmlspecialchars($value).'" style="width: 100px;" /> <br>';
    $value  = get_post_meta($post->ID, 'moneda', true);
    echo '<label>Tipo de Moneda</label><br>
    <input type="text" name="moneda" id="moneda" value="'.htmlspecialchars($value).'" style="width: 40px;" /><br> ';
    $value  = get_post_meta($post->ID, 'urlproducto', true);
    echo '<label>Ingrese URL de Destino</label><br>
    <input type="text" name="urlproducto" id="urlproducto" value="'.htmlspecialchars($value).'" style="width: 250px;" /> <br>';
    
    
}

add_action('save_post', 'guardar_precio');
add_action('publish_post', 'guardar_precio');
function guardar_precio() {
global $wpdb, $post;
if (!$post_id) $post_id = $_POST['post_ID'];
if (!$post_id) return $post;
$price= $_POST['precio'];
update_post_meta($post_id, 'precio', $price);
}

//SIMBOLO PRECIO
function save_moneda() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $moneda= $_POST['moneda'];
    update_post_meta($post_id, 'moneda', $moneda);
}
add_action('publish_post', 'save_moneda');



add_action('save_post', 'guardar_moneda');
add_action('publish_post', 'guardar_moneda');
function guardar_moneda() {
global $wpdb, $post;
if (!$post_id) $post_id = $_POST['post_ID'];
if (!$post_id) return $post;
$price= $_POST['moneda'];
update_post_meta($post_id, 'moneda', $price);
}

//URL


function save_linkproducto() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $moneda= $_POST['urlproducto'];
    update_post_meta($post_id, 'urlproducto', $moneda);
}
add_action('publish_post', 'save_linkproducto');



add_action('save_post', 'guardar_linkproducto');
add_action('publish_post', 'guardar_linkproducto');
function guardar_linkproducto() {
global $wpdb, $post;
if (!$post_id) $post_id = $_POST['post_ID'];
if (!$post_id) return $post;
$price= $_POST['urlproducto'];
update_post_meta($post_id, 'urlproducto', $price);
}


//titulo
function save_title() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $titulo= $_POST['titulo'];
    update_post_meta($post_id, 'titulo', $titulo);
}
add_action('publish_post', 'save_title');



add_action('save_post', 'guardar_titulo');
add_action('publish_post', 'guardar_titulo');
function guardar_titulo() {
global $wpdb, $post;
if (!$post_id) $post_id = $_POST['post_ID'];
if (!$post_id) return $post;
$titulo= $_POST['titulo'];
update_post_meta($post_id, 'titulo', $titulo);
}


//Tipo de producto
function save_tproducto() {
   global $wpdb, $post;
    if (!$post_id) $post_id = $_POST['post_ID'];
    if (!$post_id) return $post;
    $moneda= $_POST['tproducto'];
    update_post_meta($post_id, 'tproducto', $moneda);
}
add_action('publish_post', 'save_tproducto');



add_action('save_post', 'guardar_tproducto');
add_action('publish_post', 'guardar_tproducto');
function guardar_tproducto() {
global $wpdb, $post;
if (!$post_id) $post_id = $_POST['post_ID'];
if (!$post_id) return $post;
$price= $_POST['tproducto'];
update_post_meta($post_id, 'tproducto', $price);
}

/* /campos personalizados */

/* Soporte para carousel */
function mdesigner_post_type() {
 	register_post_type( 'mdesigner_slider',
 		array(
	      'labels' => array(
	        'name' => __( 'Carousel' ),
	        'singular_name' => __( 'Item' ),
	        'add_new' => __( 'Nuevo item' ),
	        'add_new_item' => __( 'Añadir nuevo item' ),
	        'edit_item' => __( 'Editar item' ),
	        'featured_image' => __( 'Imagen del slide' )
	      ),
	      'public' => true,
	      'exclude_from_search' => true,
	      'has_archive' => false,
	      'show_in_nav_menus' => false,
	      'menu_icon' => 'dashicons-images-alt2',
	      //'rewrite' => array('slug' => 'carousel'),
	      'supports' => array('title', 'editor', 'thumbnail')
    	)
  	);
 }
 add_action( 'init', 'mdesigner_post_type' );
/* /Soporte para carousel*/


function registra_mis_widgets() {
    register_widget('card_widget');
}
