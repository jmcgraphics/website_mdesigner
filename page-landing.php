<?php
/*Template Name: Landing page */
?>

    <?php get_header('fija'); ?>
    <!-- content-->
    <div class="container" id="section1">
        <!--llamar pagina especifica-->
        <?php 
                    $args = array (
                    'post_type'=> 'page',
                    'title' => 'Que-es-mdesigner',    
                    );
              $recent = new WP_Query($args); while($recent->have_posts()) : $recent->the_post();?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        <!-- /llamar pagina especifica-->
    </div>
    <br>
    <div class="content">
        <div class="shadow-1" id="section2">
            <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <div class="container">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="row " id="section3">
            <!--llamar pagina especifica-->
            <?php 
                $args = array (
                'post_type'=> 'page',
                'title' => 'section2',    
                );
            $recent = new WP_Query($args); 
            if($recent->have_posts()){      
            while($recent->have_posts()) : $recent->the_post(); 
            the_content(); 
            endwhile; 
            }else{ ?>

            <!--  widgets area -->
            <div class="col">
                <?php if( !function_exists('dynamic_sidebar') || !dynamic_sidebar('slider_widget1')):?>
                <?php endif; ?>
            </div>
            <div class="col">
                <?php if( !function_exists('dynamic_sidebar') || !dynamic_sidebar('slider_widget2')):?>
                <?php endif; ?>
            </div>
            <div class="col">
                <?php if( !function_exists('dynamic_sidebar') || !dynamic_sidebar('slider_widget3')):?>
                <?php endif; ?>
            </div>
            <?php }?>
            <!-- /widgets area -->
            <!-- /llamar pagina especifica-->

        </div>
        <br>
    </div>
    <div class="container" id="carousel">
        <div class="row">
            <div class="col-8 hidden-sm-down offset-2">


                <?php 
            $args = array(
            'post_type' => 'mdesigner_slider',
            'post_per_page' => 3    
            );
        $loop = new WP_Query( $args );
        
        if ($loop->have_posts()) : ?>
                <div id="mdesigner_slides" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                    $l =$loop->post_count;
                    for ($i = 0; $i < $l; $i++){ ?>
                        <li data-target="#mdesigner_slides" data-slide-to="<?php echo $i; ?>" <?php if ($i==0 ){ ?> class="active"
                            <?php }?>>
                        </li>
                        <?php
                }
                ?>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                  $n = 0;
                  while ($loop->have_posts() ): $loop->the_post(); ?>
                            <div class="carousel-item  <?php if (  $n == 0){ echo 'active'; } ?>">

                                <?php echo get_the_post_thumbnail( $loop->ID,'mdesigner_featured_image',array( 'class' => 'd-block  mh-100' )); ?>
                                <div class="carousel-caption d-none d-md-block">
                                    <p>
                                        <?php the_title(); ?>
                                    </p>
                                    <p>
                                        <?php the_content(); ?>
                                    </p>

                                </div>
                            </div>
                            <?php
                  $n++;
                  endwhile;
                  ?>
                    </div>
                </div>
                <?php
        endif;
        ?>

            </div>
        </div>


    </div>
    <div id="sec4" class="shadow-1">
        <div class="container">
            <div class="row " id="section4">
                <div class="col-xs-12 col-md-6 offset-md-3">
                    <!--llamar pagina especifica-->
                    <?php 
                $args = array (
                'post_type'=> 'page',
                'title' => 'Contacto',    
                );
          $recent = new WP_Query($args); while($recent->have_posts()) : $recent->the_post();?>
                    <?php the_content(); ?>
                    <?php endwhile; ?>
                    <!-- /llamar pagina especifica-->
                </div>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
