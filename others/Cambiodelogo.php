<?php
/* Cambiar logo de inicio de session */
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a,
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri();
            ?>/img/login-logo.png);
            height: 65px;
            width: 300px;
            background-size: auto;
            background-repeat: no-repeat;
            padding-bottom: 0.5rem;
            margin-top: 0.2rem;
        }

    </style>
    <?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

/* /Cambiar logo de inicio de session */
