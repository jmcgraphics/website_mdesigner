<?php
	
// La nueva clase debe heredar de WP_Widget
// La nueva clase debe heredar de WP_Widget
class card_widget extends WP_Widget {
    function card_widget() {
       $options = array('classname' => 'card',
                        'description' => 'muestra tarjetas de servicios para mdesigner theme');
       $this->WP_Widget('ofertas_widget', 'Tarjetas pagina estatica', $options);
    }
   function form($instance) {
        // Valores por defecto
        $defaults = array('titulo' => '', 'descripcion'=> '', 'url' => '', 'imagen' => '');
        // Se hace un merge, en $instance quedan los valores actualizados
        $instance = wp_parse_args((array)$instance, $defaults);
        // Cogemos los valores
        $titulo = $instance['titulo'];
        $subtitulo = $instance['subtitulo'];
        $descripcion = $instance['descripcion'];
        $url = $instance['url'];
        $txtbtn = $instance['txtbtn'];
        $imagen = $instance['imagen'];
        // Mostramos el formulario
       
        ?>
        <p>
            Titulo
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('titulo');?>"
                   value="<?php echo esc_attr($titulo);?>"/>
        </p>
        <p>
            Imagen
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('imagen');?>"
               value="<?php echo esc_attr($imagen);?>"/>
           
        </p>
        <p>
            Subtitulo
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('subtitulo');?>"
                   value="<?php echo esc_attr($subtitulo);?>"/>
        </p>
        <p>
            Descripcion
            <input class="widefat" style="height:60px;"type="text" name="<?php echo $this->get_field_name('descripcion');?>"
                   value="<?php echo esc_attr($descripcion);?>"/>
        </p>
        <p>
            URL
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('url');?>"
                   value="<?php echo esc_attr($url);?>"/>
        </p>
        <p>
            Texto del boton 
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('txtbtn');?>"
                   value="<?php echo esc_attr($txtbtn);?>"/>
        </p>
        
        <?php
    }
   function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Con sanitize_text_field elimiamos HTML de los campos
        $instance['titulo'] = sanitize_text_field($new_instance['titulo']);
       $instance['subtitulo'] = sanitize_text_field($new_instance['subtitulo']);
        $instance['descripcion'] = sanitize_text_field($new_instance['descripcion']);
        $instance['url'] = sanitize_text_field($new_instance['url']);
       $instance['imagen'] = sanitize_text_field($new_instance['imagen']);
       $instance['txtbtn'] = sanitize_text_field($new_instance['txtbtn']);
        return $instance;
    }
  function widget($args, $instance) {
        // Extraemos los argumentos del area de widgets
        extract($args);
        $titulo = apply_filters('widget_title', $instance['titulo']);
        $subtitulo = $instance['subtitulo'];
        $descripcion = $instance['descripcion'];
        $url = $instance['url'];
        $txtbtn = $instance['txtbtn'];
        $imagen = $instance['imagen'];
      
        echo $before_widget;
        ?>
        <div class="card text-center elevation-1">
            <div class="img-precios container">
               <div class="row justify-content-center">
                <?php 
                echo $before_title;
                echo $titulo;
                echo $after_title;
                ?>    
                </div>
                <div class="row justify-content-center">
                <img class="card-img-top" src="<?php echo $imagen;?>" alt="<?php echo $titulo; ?>">    
                </div>     
            </div>
            <div class="card-block">
                <h5 class="card-title">
                    <?php echo $subtitulo;?>
                </h5>
                <p class="card-text">
                    <?php echo $descripcion;?>
                </p>
                <a class="btn " href="<?php echo $url;?>">
                    <?php echo $txtbtn;?>
                </a>
            </div>
        </div>
       <?php
        echo $after_widget;
    }
}


    