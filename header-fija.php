<?php
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Favicon -->
    <?php if ( get_theme_mod('site_favicon') ) : ?>
    <link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod('site_favicon')); ?>" />
    <?php endif; ?>
    <!--/Favicon -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_directory_uri() );?>/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_directory_uri() );?>/css/style.min.css" type="text/css" />
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TCVXLP6');

    </script>
    <!-- End Google Tag Manager -->


    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCVXLP6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <!-- Header -->

    <header id="header-container" class="shadow-2" style="background-image: url(<?php header_image(); ?>);">

    </header>

    <nav class="navbar ppal navbar-toggleable-md fixed-top navbar-light  ">
        <div class="container">
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            <?php
                    // Display the Custom Logo
                    the_custom_logo();
                    // No Custom Logo, just display the site's name
                    if (!has_custom_logo()) {
                        ?>
                <a href="<?php echo esc_url( home_url() ); ?>" class="navbar-brand">
                    <?php bloginfo('name'); ?>
                </a>
                <?php
                    }
                    ?>
                    <div class="collapse navbar-collapse justify-content-end toggle-mobile slide-out-mobile" id="navbarNav">


                        <?php
                    wp_nav_menu( array(
                        'menu'              => 'primary',
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => '',
                        'container_class'   => ' ',
                        'container_id'      => 'navbarCollaps',
                        'menu_class'        => 'navbar-nav',
                        'fallback_cb'       => '__return_false',
                        'items_wrap'		=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'				=> 2,
                        'walker'            => new bootstrap_4_walker_nav_menu())
                    );
                ?>
                    </div>
        </div>

    </nav>

    <!-- /Header -->
