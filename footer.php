<div class="cookiebox">
    <div class="infocookies">
        <p>Esta web utiliza cookies para mejorar la navegación de sus usarios, si sigue navegando daremos por entendido que los acepta.
            <a onclick="aceptar_cookies();" style="cursor:pointer;">ok</a>
            <a href="<?php echo get_template_directory_uri(); ?>/politica-de-cookies">Más</a>

        </p>
    </div>
</div>
<footer>

    <div class="container">
        <div class="row">
            <div class="col">
                <?php
                    wp_nav_menu( array(
                        'menu'              => 'secundary',
                        'theme_location'    => 'secundary',
                        'depth'             => 2,
                        
                        'fallback_cb'       => '__return_false',
                        'items_wrap'		=> '<ul id="%1$s" class="%2$s nav justify-content-end">%3$s</ul>',
                        'depth'				=> 2,
                        'walker'            => new bootstrap_4_walker_nav_menu())
                    );
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <p>Diseñado por: M-Designer &copy; 2017 Todos los derechos reservados <a href="https://www.facebook.com/mdesigner.co"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="mdesigner-facebook"></a>
                    <a href="https://www.pinterest.com/mdesigner_co"><img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.png" alt="mdesigner-pinterest"></a>
                    <a href="https://www.instagram.com/mdesigner.co"><img src="<?php echo get_template_directory_uri(); ?>//img/instagram.png" alt="Mdesigner-instagram"></a></p>
            </div>

        </div>
    </div>
</footer>
<!-- /content -->
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>
<?php wp_footer();?>
</body>

</html>
