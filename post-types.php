<?php
/*
Template Name: productos
Template Post Type: post
*/
?>



<?php get_header('fija') ?>

<div class="container content">
    <div class="row">
        <div class="col-xs-12 col-md-6">
                <?php if (have_posts()) : while (have_posts()) : the_post();?>
            <div class="card-group">
                <div class="card text-center">              
                    <?php if ( has_post_thumbnail() );  { the_post_thumbnail( 'product-image', array('class' => 'card-img-top') ); } ?>
                    <div class="card-body">
                        <h2 class="card-title">
                           <?php the_title();?> 
                        </h2>
                        <p class="card-text">
                            <?php the_content();?>
                        </p>
                        <p class="price">
                            <?php 
                            $metas = get_post_meta($post->ID, 'price');  
                            if (isset($metas[0])) echo '<h3>Precio: '.$metas[0].'</h3>'; ?>
                        </p>
                    </div> 
                    <div class="card-footer">
                        <small class="text-muted">
                            <?php 
                                $time = human_time_diff( get_the_time('U') , current_time('timestamp') );
                                echo sprintf( __( 'hace %s', 'dominio' ), $time );
                            ?>
                        </small>
                    </div> 
                </div>  
            </div>     
                 <?php endwhile; endif; ?>
          
              
        </div>
    </div>
</div>

<?php get_footer() ?>