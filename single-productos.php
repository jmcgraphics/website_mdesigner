<?php
/*
Template Name: productos
Template Post Type: post
*/
?>
    <?php get_header('productos') ?>

    <div class="container mt-5">
        <div class="row justify-content-center mb-3">
            <div class="col-xs-12">
                <h1 class="title-producto">
                    <?php the_title();?> </h1>
            </div>
        </div>
        <div class="row justify-content-center mb-3 product">
            <div class="col-xs-12 col-md-6 ">
                <?php if (have_posts()) : while (have_posts()) : the_post();?>
                <div class="card-group">
                    <div class="card ">
                        <?php if ( has_post_thumbnail() );  { the_post_thumbnail( 'product-image', array('class' => 'card-img-top img-fluid') ); } ?>
                        <div class="card-body">
                            <div class="card-title">
                                <h2>
                                    <?php echo get_post_meta($post->ID, 'tproducto', true);?>
                                    <?php  the_title();?> </h2>
                            </div>
                            <div class="card-text">
                                <?php the_content();?>
                            </div>
                            <a href="<?php echo get_post_meta($post->ID, 'urlproducto', true);?>" class="btn">
                                <?php echo get_post_meta($post->ID, 'titulo', true);?> <span class="badge badge-success"><?php echo get_post_meta($post->ID, 'precio', true); echo get_post_meta($post->ID, 'moneda', true);?></span>
                            </a>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">
                            <?php 
                                $time = human_time_diff( get_the_time('U') , current_time('timestamp') );
                                echo sprintf( __( 'hace %s', 'dominio' ), $time );
                            ?>
                        </small>
                        </div>
                    </div>
                </div>
                <?php endwhile; endif; ?>


            </div>


        </div>
        <div class="row justify-content-center">
            <div class="col-xs-12 mb-2">
                <h3>Otros Productos para
                    <?php the_category(',');?>
                </h3>
            </div>

        </div>
        <div class="row justify-content-center">


            <?php
                $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'orderby'=>'rand', 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
                
                if( $related ) foreach( $related as $post ) {
                setup_postdata($post); ?>
                <div class="col-xs-12 col-md-3">
                    <div class="card-group  mb-3">
                        <a target="_blank" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                            <?php the_post_thumbnail('product-image-relast', array('class' => 'card-img-top  htop'));?>
                        </a>
                        <h2 class="card-title relast-title mt-3">
                            <?php the_title(); ?>
                        </h2>
                        <div class="card-text relast-text">
                            <?php the_excerpt(); ?>
                            <small class="text-muted">
                            <?php 
                                $time = human_time_diff( get_the_time('U') , current_time('timestamp') );
                                echo sprintf( __( 'hace %s', 'dominio' ), $time );
                            ?>
                        </small>

                        </div>
                        <div class="row ml-auto mr-auto mt-3">
                            <a href="<?php echo get_permalink($post->ID);?>" class="btn thumbnail mr-3">Ver 
                        </a>
                            <a href="<?php echo get_post_meta($post->ID, 'urlproducto', true);?>" class="btn thumbnail">
                                <?php echo get_post_meta($post->ID, 'titulo', true);?> <span class="badge"><?php echo get_post_meta($post->ID, 'precio', true); echo get_post_meta($post->ID, 'moneda', true);?></span>
                            </a>
                        </div>

                    </div>
                </div>
                <?php }
                    wp_reset_postdata(); ?>

        </div>



    </div>

    <?php get_footer() ?>
